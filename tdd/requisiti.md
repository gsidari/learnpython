Requisiti per Game of Life
==========================

_[Descrizione e esempi](https://it.wikipedia.org/wiki/Gioco_della_vita)_

Cellula
-------

1. una cella __nasce__ se è vuota e ci sono esattamente 3 adiacenti vive
2. una cella __sopravvive__ se è viva e ci sono 2 o 3 adiacenti vive
3. una cella __muore__ se è viva e ci sono meno di 2 o più di tre adiacenti vive

```

.0.
..0
000

 |
 V

...
0.0
..0

 |
 V

...
.0.
.0.

```

Visualizzazione
---------------

1. Ha H righe e N colonne
2. Ad ogni passo chiede se uscire o continuare