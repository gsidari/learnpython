# Aggiungiamo al PATH la cartella superiore
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import cella
import pytest

def test_sopravvive_con_2():
  assert cella.sopravvive(2, True) == True

def test_sopravvive_con_3():
  assert cella.sopravvive(3, True) == True

def test_muore_oltre_3():
  assert cella.sopravvive(4, True) == False

def test_muore_sotto_2():
  assert cella.sopravvive(1, viva=True) == False

def test_nasce_con_3():
  assert cella.nasce(3, viva=False) == True

def test_non_nasce_con_2():
  assert cella.nasce(2, viva=False) == False

def test_prossima_

