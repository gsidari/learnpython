from flask import Blueprint, render_template, request
import pickle
import pandas as pd
import os
from sklearn import ensemble, tree, linear_model
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.utils import shuffle

mod_regressione = Blueprint('mod_regressione', __name__, template_folder='../templates')

@mod_regressione.route('/stima/')
def stima():
    cartella_dati = os.path.dirname(os.path.dirname(__file__))
    X_test = pd.read_csv(cartella_dati + '/dati/X_test.csv')
    X_test.drop(['Unnamed: 0'], axis=1, inplace=True)
    modello = None
    with open(cartella_dati + '/dati/modello_lineare.pickle', 'rb') as file:
        modello = pickle.load(file)
    test_pre = modello.predict(X_test)
        
    return str(test_pre[0])
    # return render_template('stima.html', valore=str(100))

@mod_regressione.route('/stima_json/', methods=['POST'])
def stima_json():
    print(request.args)
    print(request.form)
    print(request.data)
    print(request.json)
    return request.args # .get('rooms')

