import websockets
import asyncio
from aiofile import async_open

print('Servizio Chat attivo')

clients = set()

@asyncio.coroutine
async def chat(ws, path):
    register(ws)
    while True:
        messaggio = await ws.recv()
        if messaggio != "":
            print(messaggio)
            # salva su file
            async with async_open("/tmp/hello.txt", 'w+') as afp:
                await afp.write(messaggio)
        if messaggio is None:
            break
        for socket in clients:
            await socket.send(messaggio)
        #yield from socket.send(messaggio)

def register(ws):
    clients.add(ws)
    print(f'Numero client ', len(clients))

server = websockets.serve(chat, '127.0.0.1', 7878)
asyncio.get_event_loop().run_until_complete(server)
asyncio.get_event_loop().run_forever()