import websockets
import asyncio

print('Servizio Echo attivo')

@asyncio.coroutine
def echo(ws, path):
    while True:
        messaggio = yield from ws.recv()
        if messaggio != "":
            print(messaggio)
        if messaggio is None:
            break
        yield from ws.send(messaggio)

server = websockets.serve(echo, '127.0.0.1', 7878)
asyncio.get_event_loop().run_until_complete(server)
asyncio.get_event_loop().run_forever()