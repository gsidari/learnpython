from flask import Flask, url_for, render_template, request
from markupsafe import escape
from .mod_regressione.controllers.controller_regressione import mod_regressione

app = Flask(__name__)
app.register_blueprint(mod_regressione, url_prefix='/regressione')

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/routes/')
def routes():
    res = ''
    for mapping in app.url_map.iter_rules():
        res = res + f"{mapping}<BR />" 
    return res

@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return f'User {escape(username)}' 

@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return 'Post %d' % post_id

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)

@app.route('/info/', methods=['POST', 'GET'])
def info():
    return render_template('info.html')

@app.route('/rotte/')
def rotte():
    res = ''
    for route in app.url_map.iter_rules():
        res = res + f"{route}<br />"
        res + str(route)
    return res
