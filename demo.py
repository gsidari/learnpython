import sys

def quadruplo(n):
    return 4 * n

if __name__ == "__main__":
    print(quadruplo(int(sys.argv[1])))